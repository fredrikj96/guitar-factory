package com.scania;

import com.scania.Guitars.ElectricGuitar;
import com.scania.Guitars.Guitars;
import com.scania.Guitars.AcousticGuitar;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Guitars> guitarCenter = new ArrayList<Guitars>();



        ElectricGuitar mustangComp = new ElectricGuitar("Fender", "Mustang Competition Blue", 1969, "Neck: black single coil pickup, Bridge: Seymour Duncan Hot Rails");

        AcousticGuitar seagullArtistMosaic = new AcousticGuitar("Seagull", "Artist Mosaic", 2015);


        guitarCenter.add(mustangComp);
        guitarCenter.add(seagullArtistMosaic);


        for (Guitars guitars:guitarCenter) {
            guitars.makeSound();
        }
    }
}