package com.scania.Guitars;

public interface Playable {
    void makeSound();
}
