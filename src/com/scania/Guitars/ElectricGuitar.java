package com.scania.Guitars;

public class ElectricGuitar extends Guitars{
    private String pickups;

public ElectricGuitar(String brand, String model, int year, String pickups) {

    super(brand, model, year);
    this.pickups = pickups;
}


    @Override
    public void makeSound() {


        System.out.println("Hello, hello, hello, how low\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello\n");

    }

}
